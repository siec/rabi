#!/bin/bash
# -s for silent mode

waitfile()
{
	if [ "$2" != '-s' ];then
		echo -n "wait $1"
		while [ ! -e "$1" ];do
			sleep 1
			echo -n "."
		done
		echo " done"
	else
		while [ ! -e "$1" ];do
			sleep 1
		done
	fi
}

output()
{
	local PREFIX="$1"
	local LINE
        local N=0
	while IFS= read LINE;do
		printf "%s\n" "$PREFIX$LINE"
                ((N++))
		if (( N > 5 ));then
			sleep 1
		else
			sleep 0.5
		fi
	done
	[ "$LINE" ] && printf "%s\n" "$PREFIX$LINE"
}

run_oneshoot()
{
	local i
	local n
	for i in ./oneshoot/* ./oneshoot/.[^.]*;do
		n=${i/.\/oneshoot\/}
		if [[ "$TEXT" =~ $n ]];then
			"$i" | output &
		fi
	done
}

run_cmd()
{
	local f
	local -a TOKENS=( $TEXT )

	[[ "$TOKENS" =~ / ]] && return
	[ -x "./cmd/$TOKENS" -a -f "./cmd/$TOKENS" ] || return

	"./cmd/${TOKENS[@]}" | output "$NICK, " &
}

run_tarabanctl()
{
	local f
	local -a TOKENS=( $TEXT )
	[ "$TOKENS" == "tarabanctl" ] || return
	case "${TOKENS[1]}" in
		restart)  BOT_STOP=restart ;;
		reboot)   BOT_STOP=reboot ;;
		shutdown) BOT_STOP=shutdown ;;
	esac
}


USER_HOOK=""
run_hook()
{
	eval "$USER_HOOK" | output
	[ "${TEXT:0:10}" = "user_hook " ] || return
	USER_HOOK="${TEXT:10}"
}

export BOT_NICK=${BOT_NICK:-Rabi}
export BOT_CHAN=${BOT_CHAN:-hedlx}

if [ "$1" == 'start' ];then
	killall -q ii
	rm -rf ii
	~/ii/ii -i ii -s chat.freenode.org -n "$BOT_NICK" &
	waitfile ii/chat.freenode.org/in "$2"
	echo "/j #$BOT_CHAN" > ii/chat.freenode.org/in 
	waitfile "ii/chat.freenode.org/#$BOT_CHAN/out" "$2"
fi

DIR="ii/chat.freenode.org/#$BOT_CHAN"

export DATE
export TIME
export NICK
export TEXT

echo "ぼくとぼくのねこはバカです" > "$DIR/in"

while read DATE TIME NICK TEXT;do
	NICK="${NICK:1:-1}"
	[ "$NICK" == "$BOT_NICK" ] && continue
	run_tarabanctl
	run_cmd
	run_oneshoot
	#run_hook
        [ "$BOT_STOP" ] && break
done > "$DIR/in" < <(tail -f -n0 "$DIR/out")

case "$BOT_STOP" in
    restart)  exec ./bot.sh ;;
    reboot)   exec ./bot.sh start ;;
    shutdown) killall -q ii; rm -rf ii ;;
esac
